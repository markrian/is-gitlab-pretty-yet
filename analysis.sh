#!/bin/bash
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail
set -o noglob

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

CI_BUILD_DATE=$(date '+%s')
CI_PROJECT_URL="${CI_PROJECT_URL:-https://gitlab.com/leipert/is-gitlab-pretty-yet}"
CI_JOB_NAME="${CI_JOB_NAME:-generate_stats}"

# Apparently eslint analysis can be very expensive, so we give node more memory
export NODE_OPTIONS="--max_old_space_size=4096"

mkdir -p repositories

function log {
>&2 printf "\\e[33m%s\\e[0m\\n" "$1"
}

function download {
  curl  --fail --location --silent --show-error "$@"
}

function indent { sed 's/^/    /' 1>&2; }

function checkout {
  if [ -d "repositories/$VERSION" ] ; then
    cd "repositories/$VERSION"
    git reset --hard
    git pull origin master
  else
    git clone --single-branch "$1" "repositories/$VERSION"
    cd "repositories/$VERSION"
  fi
}

function set_vars {
  cd "repositories/$VERSION"
  HASH=$(git rev-parse HEAD)
  TMP_DIR="$DIR/tmp/$HASH"
  TIME=$(git show -s --format=%ct)
  RESULT_DIR="$DIR/results/$VERSION/$TIME"
  cd "$DIR"
  rm -rf "$TMP_DIR"
  mkdir -p "$TMP_DIR"
  mkdir -p "$RESULT_DIR"
}

function install {
  cd "repositories/$VERSION"
  yarn install --pure-lockfile
  cd "$DIR"
}

function eslint {
  cd "repositories/$VERSION"
  node_modules/.bin/eslint . --ext .js,.vue -f "$DIR/slim-json-formatter.js" \
    --no-inline-config --report-unused-disable-directives \
    > "$TMP_DIR/eslint.json" || echo ""
  cd "$DIR"
}

function analysis_eslint {
  local pretty
  local total
  local top20
  local errors
  local warnings
  pretty=0

  log "Parsing eslint results"

  total='
    sort_by(.errorCount, .warningCount, .filePath) | reverse |
      .[] | [.errorCount, .warningCount, .filePath ] | map(tostring) |
      join("\t")
  '

  jq -r "$total" "$TMP_DIR/eslint.json" | sed "s#/.*repositories/$VERSION/##g" > "$RESULT_DIR/eslint-files.txt"

  log "Aggregating eslint results"

  # shellcheck disable=SC2016
  top20='
    [.[].messages[].ruleId] |
      reduce .[] as $i ({}; setpath([$i]; getpath([$i]) + 1) ) |
      to_entries |
      sort_by(.value) | reverse | .[0:20] |
      from_entries
  '

  top20=$(jq "$top20" "$TMP_DIR/eslint.json")

  total=$(wc -l < "$RESULT_DIR/eslint-files.txt")
  pretty=$(grep -c "^0\\t0" "$RESULT_DIR/eslint-files.txt")
  errors=$(awk '{ sum += $1 } END { print sum + 0 }' "$RESULT_DIR/eslint-files.txt")
  warnings=$(awk '{ sum += $2 } END { print sum + 0 }' "$RESULT_DIR/eslint-files.txt")

  log "Eslint summary"

  echo '{}' \
    | jq --arg total "$total" '.total = $total' \
    | jq --arg pretty "$pretty" '.pretty = $pretty' \
    | jq --arg errors "$errors" '.errors = $errors' \
    | jq --arg warnings "$warnings" '.warnings = $warnings' \
    | jq --arg 'hash' "$HASH" '.commit = $hash' \
    | jq --arg 'version' "$VERSION" '.version = $version' \
    | jq --arg 'time' "$TIME" '.time = $time' \
    | jq --argjson 'top20' "$top20" '.top20 = $top20' \
    | tee "$RESULT_DIR/eslint-metadata.json"

  log "Downloading history from gitlab CI"

  download --output "$TMP_DIR/eslint-history.txt" \
    "https://leipert-projects.gitlab.io/is-gitlab-pretty-yet/$VERSION/eslint-history.txt"

  if ! grep -q "$HASH" "$TMP_DIR/eslint-history.txt" ; then
    printf "%s\\t%s\\t%s\\t%s\\t%s\\t%s\\n" "$CI_BUILD_DATE" "$total" "$pretty" "$errors" "$warnings" "$HASH" >> "$TMP_DIR/eslint-history.txt"
  fi

  sort -n -k1 "$TMP_DIR/eslint-history.txt" > "$RESULT_DIR/eslint-history.txt"

  tail -n 10 "$RESULT_DIR/eslint-history.txt"

}

function update {
  log "Checking out gitlab-$VERSION"
  checkout "$1" 2>&1 | indent
}

function historic_jest_entry {
  local timestamp=${1:-NOW}
  local jest_files
  local karma_files
  local EE_COMMIT
  local epoch

  if [[ "$timestamp" == "NOW" ]]; then
    epoch=$(date '+%s')
    EE_COMMIT=$(git log --pretty="%H" -1)
  else
    epoch=$(date -d "$1" '+%s')
    EE_COMMIT=$(git rev-list --max-count=1 --first-parent --until="$1" origin/master)
  fi
  jest_files=$(git ls-tree --name-only -r "$EE_COMMIT" -- spec/frontend ee/spec/frontend | grep -c spec.js || true)
  karma_files=$(git ls-tree --name-only -r "$EE_COMMIT" -- spec/javascripts ee/spec/javascripts | grep -c spec.js || true)
  printf "%s\\t%s\\t%s\\t%s\\n" "$epoch" "$karma_files" "$jest_files" "$EE_COMMIT"
}

function seed_jest_history {
  local day
  local end
  day=$(date -u --iso-8601=seconds -d "$1")
  end=$(date -u --iso-8601=seconds -d "- 1 hours")
  while [[ "$day" < "$end" ]]; do
    historic_jest_entry "$day"
    day=$(date -u --iso-8601=seconds -d "$day + 12 hours")
  done
}

function analysis_jest {
  local tmpfile
  tmpfile="$TMP_DIR/jest-v2.txt"
  cd repositories/ee

  download --output "$tmpfile" \
    "https://leipert-projects.gitlab.io/is-gitlab-pretty-yet/jest/jest-v2.txt" \
    || rm -rf "$tmpfile"

  if [[ ! -f "$tmpfile" ]]; then
    log "No history available. Seeding the history now"
    touch "$tmpfile"
    seed_jest_history "2019-01-01T00:00:00Z" | tee -a "$tmpfile"
  fi

  echo "Getting last result"

  historic_jest_entry | tee -a "$tmpfile"

  RESULT_DIR="$DIR/static/jest"
  rm -rf "$RESULT_DIR"
  mkdir -p "$RESULT_DIR"
  mv "$tmpfile" "$RESULT_DIR"

}

function run_pipeline {
  set_vars
  log "Installing dependencies"
  install 2>&1 | indent
  log "Running eslint"
  eslint 2>&1 | indent
  log "Formatting eslint results properly"
  analysis_eslint 2>&1 | indent
  log "Analysis of jest/karma ratio"
  analysis_jest 2>&1 | indent
  log "Copying results to public dir"
  mkdir -p "static/$VERSION"
  cp -r "$RESULT_DIR/." "static/$VERSION"
  log "Finished running analysis on gitlab-$VERSION"
}

function compile_static {
  cd "$DIR"
  log "Copying static page and zipping"
  yarn run generate 2>&1 | indent
  find dist -type f -print0 | xargs -0 gzip -f -k
  log "Finished build"
  tree dist/ | indent
}

log "Check if everything is alright"
yarn install 2>&1 | indent
yarn run lint 2>&1 | indent

HASH=
TMP_DIR=
RESULT_DIR=
TIME=

if [ "${CI:=false}" == "true" ]; then
  log "Overwriting build date"
  echo "{}" \
    | jq --argjson buildDate "$CI_BUILD_DATE" '.buildDate = $buildDate' \
    | jq --arg repoUrl "$CI_PROJECT_URL" '.repoUrl = $repoUrl' \
    > assets/data.json
else
  log "Not in CI environment, not overwriting build date"
fi

VERSION=ee
update https://gitlab.com/gitlab-org/gitlab.git
run_pipeline

compile_static

