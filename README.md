# Is GitLab pretty yet?

This repo runs several script to track our progress on improving the Code Quality of GitLab:

- eslint: https://leipert-projects.gitlab.io/is-gitlab-pretty-yet/eslint/
- prettier: https://leipert-projects.gitlab.io/is-gitlab-pretty-yet/prettier/
- CE/EE diff: https://leipert-projects.gitlab.io/is-gitlab-pretty-yet/diff/

The pages are generated about [every 4 hours](https://gitlab.com/leipert-projects/is-gitlab-pretty-yet/pipeline_schedules).

## Prerequisites

- git
- jq
- bash
- gzip
- awk
- curl
- node

## Local setup for development

```bash
# Run analysis once, this should be enough to do the local development
$ bash ./analysis.sh

# Above does yarn install already, so you should be able to do start the development server
$ yarn run dev
```
