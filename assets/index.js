import axios from 'axios';
import moment from 'moment';

function percent(pretty, total) {
  return Math.round((pretty / total) * 10000) / 100;
}

const BASE_URL = 'https://gitlab.com/gitlab-org/gitlab';

const parseResults = response => {
  const project = response.data;

  project.percent = percent(project.pretty, project.total);

  project.url = project.version === 'ee' ? BASE_URL : BASE_URL + '-foss';
  project.commitUrl = project.url + '/tree/' + project.commit;
  project.name = 'GitLab ' + project.version.toLocaleUpperCase();
  project.commitTime = new Date(project.time * 1000);
  project.files = [];

  return project;
};

export const retrieveJestMetadata = () => {
  return axios.get('./jest/jest-v2.txt').then(({ data: results }) => {
    const jest = [];
    const karma = [];

    results.split('\n').forEach(function(line) {
      if (line) {
        const chunks = line.split('\t');
        const date = new Date(parseInt(chunks[0], 10) * 1000);
        const karmaCount = parseInt(chunks[1], 10);
        const jestCount = parseInt(chunks[2], 10);
        const commit = chunks[3];
        const total = karmaCount + jestCount;

        const data = {
          jest: jestCount,
          karma: karmaCount,
          date,
          commit,
          total,
          afterBody: [
            'Total files: ' + total,
            'Foolishness: ' + ((100 * jestCount) / total).toFixed(2) + '%',
            'Commit: ' + commit.substring(0, 10),
          ],
        };

        jest.push({
          data,
          x: date,
          y: jestCount,
        });

        karma.push({
          data,
          x: date,
          y: karmaCount,
        });
      }
    });

    const last = jest[jest.length - 1].data;

    const lastCommit = last.commit;

    return {
      percent: percent(last.jest, last.total),
      url: BASE_URL,
      commit: lastCommit,
      commitUrl: BASE_URL + '/tree/' + lastCommit,
      name: 'GitLab',
      commitTime: last.date,
      last,
      dataset: { jest, karma },
    };
  });
};

export const retrievePrettierMetadata = key => {
  return axios.get('./' + key + '/metadata.json').then(parseResults);
};

export const retrieveEslintMetadata = key => {
  return axios.get('./' + key + '/eslint-metadata.json').then(parseResults);
};

const minSize = 5;
const maxSize = 25;

function scale(position, maxValue) {
  return Math.max(
    Math.round((Math.log(position) / Math.log(maxValue)) * maxSize),
    minSize
  );
}

const retrieveFilesForEslint = key => {
  return axios.get('./' + key + '/eslint-files.txt').then(function(response) {
    let files = [];

    response.data.split('\n').forEach(function(line) {
      if (line) {
        const chunks = line.split('\t');
        const errors = parseInt(chunks[0], 10);
        const warnings = parseInt(chunks[1], 10);
        const dim = scale(errors + warnings, 300);

        files.push({
          errors: errors,
          warnings: warnings,
          name: chunks[2],
          class: errors ? 'red' : warnings ? 'yellow' : 'green',
          dim: dim,
          title:
            chunks[2] + '\n' + errors + ' Errors\n' + warnings + ' Warnings',
        });
      }

      files = files.sort(function(a, b) {
        return b.dim - a.dim;
      });
    });

    return files;
  });
};

export const retrieveFiles = (key, target) => {
  switch (target) {
    case 'eslint':
      return retrieveFilesForEslint(key);
    default:
      throw new Error(`retrieveFiles: Unknown target ${target}`);
  }
};

const retrieveHistoryForPrettier = key => {
  return axios.get('./' + key + '/history.txt').then(function(response) {
    const percentage = [];
    const totalLines = [];

    response.data.split('\n').forEach(function(line) {
      if (line) {
        const chunks = line.split('\t');
        const timestamp = moment(parseInt(chunks[0], 10) * 1000);
        const totalFiles = parseInt(chunks[1], 10);
        const prettyFiles = parseInt(chunks[2], 10);
        const lineDiff = parseInt(chunks[3], 10);

        const data = {
          timestamp: timestamp,
          prettyFiles: prettyFiles,
          totalFiles: totalFiles,
          lineDiff: lineDiff,
          afterBody: [
            'Total files: ' + totalFiles,
            'Pretty files: ' + prettyFiles,
            'Commit: ' + chunks[4].substring(0, 10),
          ],
        };

        if (totalFiles > 0) {
          percentage.push({
            x: timestamp,
            y: percent(prettyFiles, totalFiles),
            data: data,
          });
          totalLines.push({ x: timestamp, y: lineDiff, data: data });
        }
      }
    });

    return {
      percentage,
      totalLines,
    };
  });
};

const retrieveHistoryForEslint = key => {
  return axios.get('./' + key + '/eslint-history.txt').then(function(response) {
    const percentage = [];
    const warnings = [];
    const errors = [];

    response.data.split('\n').forEach(function(line) {
      if (line) {
        const chunks = line.split('\t');
        const timestamp = moment(parseInt(chunks[0], 10) * 1000);
        const totalFiles = parseInt(chunks[1], 10);
        const prettyFiles = parseInt(chunks[2], 10);
        const errorCount = parseInt(chunks[3], 10);
        const warningsCount = parseInt(chunks[4], 10);

        const data = {
          timestamp: timestamp,
          prettyFiles: prettyFiles,
          totalFiles: totalFiles,
          errors: errorCount,
          warnings: warningsCount,
          afterBody: [
            'Total files: ' + totalFiles,
            'Pretty files: ' + prettyFiles,
            'Commit: ' + chunks[5].substring(0, 10),
          ],
        };

        if (totalFiles > 0) {
          percentage.push({
            x: timestamp,
            y: percent(prettyFiles, totalFiles),
            data: data,
          });
          errors.push({ x: timestamp, y: errorCount, data: data });
          warnings.push({ x: timestamp, y: warningsCount, data: data });
        }
      }
    });

    return {
      percentage,
      warnings,
      errors,
    };
  });
};

const retrieveHistoryForDiff = () => {
  return axios.get('./diff/diff-history-final.txt').then(function(response) {
    const diffFiles = [];
    const additions = [];
    const deletions = [];

    response.data.split('\n').forEach(function(line) {
      if (line) {
        const chunks = line.split('\t');
        const timestamp = moment(parseInt(chunks[0], 10) * 1000);
        const files = parseInt(chunks[1], 10);
        const adds = parseInt(chunks[2], 10);
        const rems = parseInt(chunks[3], 10);

        const data = {
          timestamp: timestamp,
          diffFiles: files,
          additions: adds,
          deletions: rems,
          afterBody: [
            'Commit CE: ' + chunks[4].substring(0, 10),
            'Commit EE: ' + chunks[5].substring(0, 10),
          ],
        };

        if (files > 0) {
          diffFiles.push({
            x: timestamp,
            y: files,
            data: data,
          });
          deletions.push({ x: timestamp, y: rems, data: data });
          additions.push({ x: timestamp, y: adds, data: data });
        }
      }
    });

    return {
      diffFiles,
      additions,
      deletions,
    };
  });
};

export const retrieveHistory = (key, target) => {
  switch (target) {
    case 'prettier':
      return retrieveHistoryForPrettier(key);
    case 'eslint':
      return retrieveHistoryForEslint(key);
    case 'diff':
      return retrieveHistoryForDiff();
    default:
      throw new Error(`retrieveHistory: Unknown target ${target}`);
  }
};
